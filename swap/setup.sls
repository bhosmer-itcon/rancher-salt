#Enables swap space on systems that are not provisioned with less than 2GB

setup_swap:
  cmd.run:
    - name: |
        [ -f /swapfile ] && swapoff /swapfile
        dd if=/dev/zero of=/swapfile bs=1M count=2048
        chmod 0600 /swapfile
        mkswap /swapfile
    - unless:
      - "[ `free -b | awk '/Swap/{print $2}'` -ge 2147479551 ]"
  mount.swap:
    - name: /swapfile
    - onlyif:
      - file /swapfile 2>&1 | grep -q "Linux/i386 swap"