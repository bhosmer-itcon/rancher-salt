1) Install salt on the master
#Installs the master and minion
sudo sh bootstrap_salt.sh -M

2) Install salt on the minion

sudo sh bootstrap_salt.sh -A {Master IP/Hostname}

3) Verify and accept the salt keys

salt-key -A

If you are sure that the master only has safe keys, use the following:
salt-key -A -y

4) Add roles to appropriate minions from the master
salt '*' grains.append role rancher_server
salt '*' grains.append role rancher_agent

5) Highstate the rancher server minion
salt -G 'role:rancher_server' state.highstate

6) Configure a new environment within the Rancher UI
Visit http://{{RANCHER_URL}:8080/settings/env

7) Retrieve the tokenized Rancher Registration URL from "Add Hosts"
Visit http://{{RANCHER_URL}:8080/ -> Infrastructure -> Hosts -> Add Host

8) Set the rancher_registration grain to the generated tokenized URL on the appropriate minions
salt '*' grains.set rancher:registration_path '{URL_FROM_RANCHER_UI_ADD_HOST}'

9) Set the Cattle IP if agent is running on rancher_server
# Single quotes are required!
salt '*' grains.set rancher:cattle_agent_ip  'IP'

9) Highstate the rancher agent minion
salt -G 'role:rancher_agent' state.highstate
