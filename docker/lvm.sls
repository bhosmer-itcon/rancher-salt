# Setup adapted from https://docs.docker.com/engine/userguide/storagedriver/device-mapper-driver/#/configure-direct-lvm-mode-for-production

{% set block_device_name = salt['grains.get']('docker:block_device_name', '/dev/vdb') %}

lvm_install:
  pkg.installed:
    - name: lvm2

docker_initialize_lvm:
  lvm.pv_present:
    - name: {{ block_device_name }} # add to locally sourced pillar
    - require:
      - pkg: lvm_install

docker_volume_group:
  lvm.vg_present:
    - devices: {{ block_device_name }} # add to locally sourced pillar
    - name: docker
    - require:
      - lvm: docker_initialize_lvm

docker_logical_volume:
  lvm.lv_present:
    - name: thinpool
    - vgname: docker
    - extents: 95%VG
    - kwargs: {
        wipesignatures y
      }
    - require:
      - lvm: docker_volume_group
    - unless: lvdisplay | grep -q 'thinpool'

docker_logical_volume_meta:
  lvm.lv_present:
    - name: thinpoolmeta
    - vgname: docker
    - extents: 1%VG
    - kwargs: {
        wipesignatures y
      }
    - require:
      - lvm: docker_volume_group
    - onchanges:
      - lvm: docker_logical_volume

docker_thinpool_settings:
  file.managed:
    - source: salt://docker/conf/etc/lvm/profile/docker-thinpool.profile
    - name: /etc/lvm/profile/docker-thinpool.profile
    - require:
      - lvm: docker_logical_volume
      - lvm: docker_logical_volume_meta

docker_thinpool_convert:
  cmd.run:
    - name: lvconvert -y --zero n -c 512K --thinpool docker/thinpool --poolmetadata docker/thinpoolmeta
    - onchanges:
      - lvm: docker_logical_volume
    - require_in:
      - pkg: docker