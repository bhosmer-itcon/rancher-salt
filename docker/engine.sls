include:
- versionlock

# Based on the install script at https://get.docker.com/
docker:
  file.managed:
    - humanname: Docker Repository
    - name: /etc/yum.repos.d/docker-ce.repo
    - source: https://download.docker.com/linux/centos/docker-ce.repo
    - skip_verify: True
    - require_in:
      - pkg: docker

  pkg.installed:
    - name: docker-ce.x86_64
    - hold: True
    - version: 17.06.0.ce-1.el7.centos
    - require:
      - pkg: versionlock
    - require:
      - pkg: docker_python_pip
      - pip: docker_python_pip

  pkgrepo.managed:
    - name: rhel-7-server-extras-rpms
    - humanname: Red Hat Enterprise Linux 7 Server - Extras (RPMs)
    - require_in:
      - pkg: docker

  service.running:
  # See https://docs.saltstack.com/en/develop/ref/states/all/salt.states.service.html
    - name: docker
    - enable: True
    - require:
      - pkg: docker

docker_service_config:
  file.managed:
    - name: /etc/systemd/system/docker.service
    - source: salt://docker/conf/etc/systemd/system/docker.service
    - makedirs: True
    - create: True
    - require_in:
      - service: docker
    - require:
      - pkg: docker

docker_python_pip:
  pkg.installed:
    - pkgs:
      - python2-pip
  pip.installed:
    - name: docker-py
