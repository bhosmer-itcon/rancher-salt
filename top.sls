base:
  'role:rancher_server':
    - match: grain
    - docker
    - rancher/server
    - swap
  'role:rancher_agent':
    - match: grain
    - docker
    - rancher/agent
    - swap