# Retrieves the rancher registration URL with token
# Set the configuration to a rancher agent minion by using:
# $ salt '*' grains.append rancher_registration "{URL}"
{% set rancher_reg = salt['grains.get']('rancher:registration_path') %}

# Retrieves CATTLE_IP information from grain
{% set cattle_ip = salt['grains.get']('fqdn', '') %}
# Sets cattle_ip to defined grains value
{%set rancher_grains = salt['grains.get']('rancher', None)%}
{% if 'cattle_agent_ip' in rancher_grains %}
  {% set cattle_ip = salt['grains.get']('rancher:cattle_agent_ip', '') %}
{% endif %}

# Require docker
include:
- docker

rancher_agent_docker-image:
  docker_image.present:
    - name: rancher/agent:v1.2.10
    - force: True

rancher_agent:
  docker_container.running:
    - name: rancher_agent
    - image: rancher/agent:v1.2.10
    - require:
      - docker_image: rancher_agent_docker-image
    - binds:
      - /var/run/docker.sock:/var/run/docker.sock:rw
      - /var/lib/rancher:/var/lib/rancher:rw
    - detach: True
    - privileged: True
    - command: {{ rancher_reg }}
# Sets Cattle Agent IP, required if running rancher agent on host
# http://docs.rancher.com/rancher/v1.2/en/hosts/custom/#adding-hosts-to-the-same-machine-as-rancher-server
  {% if cattle_ip %}
    - environment:
      - CATTLE_AGENT_IP: {{ cattle_ip }}
  {% endif %}