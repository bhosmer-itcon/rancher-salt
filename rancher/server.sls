# Require docker
include:
- docker

rancher_server_docker-image:
  docker_image.present:
    - name: rancher/enterprise:v1.6.16
    - force: True

rancher_server:
  docker_container.running:
    - name: rancher_service
    - image: rancher/enterprise:v1.6.16
    - require:
      - docker_image: rancher_server_docker-image
    - port_bindings:
      - 8080:8080
    - detach: True
    - restart_policy: unless-stopped
    - binds:
      - /app/system/rancher/server/var/lib/mysql:/var/lib/mysql
